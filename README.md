### Philosophy
---
- Keep it simple.
- Be efficient, and don't kill every PC you meet.

### Notes
---
- The main menu is listed, similar to Left 4 Dead 1.
- Selecting a gamemode will instantly lead to the "Advanced Search" page, "Custom Search" button replaced with a "Create Lobby" button.
	- "Play Single Player" will list all of the relevant modes.
	- "Extras" is repurposed as a menu for the less-played online modes.
		- Scavenge, Realism, Realism Versus, and Versus Survival.
- The "Achievements" and "Blog Post" buttons are removed.
- The "Quit" button will instantly quit the game using the same method I did in "Quit Confirmation Disabler".
- The addons submenu is sleeker and shows more addons.

### Installation
---
- Official Releases
	- Subscribe to the mod on the [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2680108321) *or* download the .VPK file [from here on GitLab](https://gitlab.com/unryker/left-4-dead-2-addons/rykers-main-menu/-/releases) and put the VPK in your `steamapps/common/Left 4 Dead 2/left4dead2/addons` folder.
- Bleeding-edge Releases
	- Download the [source code](https://gitlab.com/unryker/left-4-dead-2-addons/rykers-main-menu) as a .ZIP here. Extract the folder inside the .ZIP to `left4dead2/addons`. Drag and drop the extracted folder onto VPK.exe inside `left 4 dead 2/bin/`. Install the newly-made .VPK file into `left 4 dead 2/left4dead2/addons`.

### Compatibility
---
- Compatible with anything as long as it doesn't directly modify the main menu, such as Urik HUD, Simple HUD, and NeunGUI.
	- Note I said Urik **HUD**, not Urik **Menu**.
- Compatible with [Ryker's Pause Menu.](https://gitlab.com/unryker/left-4-dead-2-addons/rykers-pause-menu)