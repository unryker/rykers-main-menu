"Resource/UI/MainMenu.res"
{
	"MainMenu"
	{
		"ControlName"			"Frame"
		"fieldName"				"MainMenu"
		"xpos"					"0"
		"ypos"					"0"
		"wide"					"f0"
		"tall"					"f0"
		"autoResize"			"0"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"PaintBackgroundType"	"0"
	}

	"BtnPlaySolo"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnPlaySolo"
		"xpos"					"100"
		"ypos"					"135"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"PnlQuickJoin"
		"navDown"				"BtnCoOp"
		"labelText"				"#L4D360UI_MainMenu_PlaySolo"
		"style"					"MainMenuButton"
		"command"				"FlmSoloFlyout"
		"ActivationType"		"1"
	}

	"FlmSoloFlyout"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmSoloFlyout"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnSoloCoOp"
		"ResourceFile"			"resource/UI/L4D360UI/SoloFlyout.res"
	}

	"BtnCoOp"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnCoOp"
		"xpos"					"100"
		"ypos"					"155"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"1"
		"navUp"					"BtnPlaySolo"
		"navDown"				"BtnVersus"
		"labelText"				"#L4D360UI_MainMenu_CoOp"
		"style"					"MainMenuButton"
		"command"				"CustomMatch_CoOp"
		"ActivationType"		"1"
	}

	"BtnVersus"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnVersus"
		"xpos"					"100"
		"ypos"					"175"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnCoOp"
		"navDown"				"BtnSurvival"
		"labelText"				"#L4D360UI_MainMenu_Versus"
		"style"					"MainMenuButton"
		"command"				"CustomMatch_versus"
		"ActivationType"		"1"
	}

	"BtnSurvival"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnSurvival"
		"xpos"					"100"
		"ypos"					"195"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnVersus"
		"navDown"				"BtnExtraModes"
		"labelText"				"#L4D360UI_MainMenu_Survival"
		"style"					"MainMenuButton"
		"command"				"CustomMatch_survival"
		"ActivationType"		"1"
	}

	"BtnExtraModes"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnExtraModes"
		"xpos"					"100"
		"ypos"					"215"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnSurvival"
		"navDown"				"BtnMutation"
		"labelText"				"#L4D360UI_MainMenu_Extras"
		"style"					"MainMenuButton"
		"command"				"ExtraModesFlyout"
		"ActivationType"		"1"
	}

	"ExtraModesFlyout"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"ExtraModesFlyout"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnScavenge"
		"ResourceFile"			"resource/UI/L4D360UI/ExtraModesFlyout.res"
	}

	"BtnMutation"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnMutation"
		"xpos"					"100"
		"ypos"					"235"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"custommode"			"1"
		"navUp"					"BtnExtraModes"
		"navDown"				"BtnOptions"
		"labelText"				"#L4D360UI_ModeCaps_CHALLENGE" // [Ryker] TODO: Figure out how to show currently selected mutation. This ignores the currently selected mutation's name even if I put the translation there.
		"style"					"MainMenuButton"
		"command"				"PlayChallenge"
		"ActivationType"		"1"
	}

	"FlmMutationCategories" // [Ryker] TODO: Figure out what this is.
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmMutationCategories"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnQuickMatch"
		"ResourceFile"			"resource/UI/L4D360UI/MutationCategoriesFlyout.res"
	}

	"FlmMutationSolo" // [Ryker] Mutation Solo Mode flyout (TODO: Find difference between this and FlmChallengeFlyout1)
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmMutationSolo"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnQuickMatch"
		"ResourceFile"			"resource/UI/L4D360UI/MutationSoloFlyout.res"
	}

	"FlmMutationCoop" // [Ryker] Mutation CoOp Mode flyout (TODO: Find difference between this and FlmChallengeFlyout4)
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmMutationCoop"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnQuickMatch"
		"ResourceFile"			"resource/UI/L4D360UI/MutationCoopFlyout.res"
	}

	"FlmMutationVersus" // [Ryker] Mutation Versus Mode flyout (TODO: Find difference between this and FlmChallengeFlyout8)
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmMutationVersus"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnQuickMatch"
		"ResourceFile"			"resource/UI/L4D360UI/MutationVersusFlyout.res"
	}

	"FlmChallengeFlyout1" // [Ryker] Mutation Solo Mode flyout duplicate(?)
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmChallengeFlyout1"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnStartGame"
		"ResourceFile"			"resource/UI/L4D360UI/mainmenu_flyout_challenge1.res"
	}

	"FlmChallengeFlyout4" // [Ryker] Mutation CoOp Mode flyout duplicate(?)
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmChallengeFlyout4"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnQuickMatch"
		"ResourceFile"			"resource/UI/L4D360UI/mainmenu_flyout_challenge4.res"
	}

	"FlmChallengeFlyout8" // [Ryker] Mutation Versus Mode flyout duplicate(?)
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmChallengeFlyout8"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnQuickMatch"
		"ResourceFile"			"resource/UI/L4D360UI/mainmenu_flyout_challenge8.res"
	}

	"BtnOptions"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnOptions"
		"xpos"					"100"
		"ypos"					"275"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnMutation"
		"navDown"				"BtnAddons"
		"labelText"				"#L4D360UI_MainMenu_Options"
		"style"					"MainMenuButton"
		"command"				"FlmOptionsFlyout"
		"ActivationType"		"1"
	}

	"FlmOptionsFlyout"
	{
		"ControlName"			"FlyoutMenu"
		"fieldName"				"FlmOptionsFlyout"
		"visible"				"0"
		"wide"					"0"
		"tall"					"0"
		"zpos"					"3"
		"InitialFocus"			"BtnVideo"
		"ResourceFile"			"resource/UI/L4D360UI/OptionsFlyout.res"
	}

	"IconAddons"
	{
		"ControlName"			"ImagePanel"
		"fieldName"				"IconAddons"
		"xpos"					"75"
		"ypos"					"295"
		"wide"					"20"
		"tall"					"20"
		"scaleImage"			"1"
		"pinCorner"				"0"
		"visible"				"0"
		"enabled"				"1"
		"tabPosition"			"0"
		"image"					"common/l4d_spinner"
		"frame"					"0"
		"scaleImage"			"1"
	}	

	"BtnAddons"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnAddons"
		"xpos"					"100"
		"ypos"					"295"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnOptions"
		"navDown"				"BtnServerBrowser"
		"labelText"				"#L4D360UI_Extras_Addons"
		"style"					"MainMenuButton"
		"command"				"Addons"
		"ActivationType"		"1"
	}

	"BtnServerBrowser"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnServerBrowser"
		"xpos"					"100"
		"ypos"					"315"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnAddons"
		"navDown"				"BtnQuit"
		"labelText"				"#L4D360UI_MainMenu_ServerBrowser"
		"style"					"MainMenuButton"
		"command"				"#openserverbrowser"
		"ActivationType"		"1"
	}

	"BtnQuit"
	{
		"ControlName"			"L4D360HybridButton"
		"fieldName"				"BtnQuit"
		"xpos"					"100"
		"ypos"					"335"
		"wide"					"180"
		"tall"					"20"
		"autoResize"			"1"
		"pinCorner"				"0"
		"visible"				"1"
		"enabled"				"1"
		"tabPosition"			"0"
		"navUp"					"BtnServerBrowser"
		"navDown"				"PnlQuickJoin"
		"labelText"				"#L4D360UI_MainMenu_Quit"
		"style"					"MainMenuButton"
		"command"				"#quit" // [Ryker] Calls the console command to quit the game, skipping the confirmation prompt.
		"ActivationType"		"1"
	}

	"PnlQuickJoin"
	{
		"ControlName"			"QuickJoinPanel"
		"fieldName"				"PnlQuickJoin"
		"ResourceFile"			"resource/UI/L4D360UI/QuickJoin.res"
		"visible"				"0"
		"wide"					"240"
		"tall"					"300"
		"xpos"					"80"
		"ypos"					"r75"
		"navUp"					"BtnQuit"
		"navDown"				"BtnPlaySolo"
	}
	
	"PnlQuickJoinGroups"
	{
		"ControlName"			"QuickJoinGroupsPanel"
		"fieldName"				"PnlQuickJoinGroups"
		"ResourceFile"			"resource/UI/L4D360UI/QuickJoinGroups.res"
		"visible"				"0"
		"wide"					"500"
		"tall"					"300"
		"xpos"					"c0"
		"ypos"					"r75"
		"navUp"					""
		"navDown"				""
	}
}